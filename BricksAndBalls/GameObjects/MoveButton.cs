﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BricksAndBalls.GameObjects
{
    class MoveButton : Button
    {
        public MoveButton(Texture2D buttonTexture,
          int xPosition,
          int yPosition)
            : base(buttonTexture,
                xPosition,
                yPosition) 
        { }
       
        public override bool WasPressed(ref TouchCollection touches)
        {
            foreach (var touch in touches)
            {
                if (touch.Id == _lastTouchId)
                {
                    _lastTouchId = touch.Id;
                    _pressed = true;
                    return true;
                }
                  
                if (touch.State != TouchLocationState.Pressed)
                    continue;
                if (_location.Contains(touch.Position))
                {
                    _lastTouchId = touch.Id;
                    _pressed = true;
                    return true;
                }
            }
            _pressed = false;
            return false; ;
        }
    }
}
