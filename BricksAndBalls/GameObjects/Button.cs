﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BricksAndBalls.Helpers;


namespace BricksAndBalls.GameObjects
{
    class Button
    {
    
        protected Rectangle _location;
        protected Texture2D _buttonTexture;
        protected int _lastTouchId;
        protected bool _pressed;

        public Button(Texture2D buttonTexture,
            int xPosition,
            int yPosition)
        {
            _buttonTexture = buttonTexture;
            _location = new Rectangle(xPosition,
                yPosition,
                _buttonTexture.Width,
                _buttonTexture.Height);
        }

        public virtual bool WasPressed(ref TouchCollection touches)
        {
            foreach (var touch in touches)
            {
                if (touch.Id == _lastTouchId)
                {
                    continue;
                }
                  
                if (touch.State != TouchLocationState.Pressed)
                    continue;
                if (_location.Contains(touch.Position))
                {
                    _lastTouchId = touch.Id;
                    _pressed = true;
                    return true;
                }
            }
            _pressed = false;
            return false; ;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_buttonTexture,
                _location,
                null,
                Color.White);
        }

   

    }
}
