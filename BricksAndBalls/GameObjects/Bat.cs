﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BricksAndBalls.GameObjects
{
    class Bat
    {

        private Texture2D _batTexture;
        private Rectangle _location;
        private float playerMoveSpeed = 8.0f;
        private Vector2 _tempPosition;

        public float MoveSpeed
        {
            get { return playerMoveSpeed; }
        }

      
        public int Width
        {
            get { return _batTexture.Width; }
        }

        public int Height
        {
            get { return _batTexture.Height; }
        }

        public void MoveLeft(int leftBounds, int rightBounds)
        {
           _location.X = MathHelper
               .Clamp(_location.X -= (int)playerMoveSpeed,
               leftBounds, rightBounds);
        }

        public void MoveRight(int leftBounds, int rightBounds)
        {
            _location.X = MathHelper
             .Clamp(_location.X += (int)playerMoveSpeed,
             leftBounds, rightBounds);
        }

        public Bat(Texture2D batTexture, int xPosition,
            int yPosition)
        {
            _batTexture = batTexture;
            
            _location = new Rectangle(xPosition,
                yPosition, _batTexture.Width,
                _batTexture.Height);
        }

        public void Initialize()
        {
             
        }

        public void Update(GameTime gameTime)
        {

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_batTexture,
                _location, null, Color.White);
        }
    }
}
