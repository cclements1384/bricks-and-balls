﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BricksAndBalls.Helpers
{
    public static class MyExtensions
    {
        public static Vector2 GetHalfSize(this Texture2D texture){
            return new Vector2(texture.Width / 2,
            texture.Height / 2);
        }

        public static int GetCenterX(this Rectangle rect)
        {
            return rect.Width / 2;
        }

        public static int GetCenterY(this Rectangle rect)
        {
            return rect.Height / 2;
        }
    }
}
