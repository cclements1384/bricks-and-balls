﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace BricksAndBalls.Helpers
{
    class DrawState
    {
        //SpriteBatch _spriteBatch;
        private readonly Rectangle _screenBounds;
        private readonly Matrix _screenXform;
       
        public Matrix ScreenXform
        {
            get { return _screenXform; }
        }

        public Rectangle ScreenBounds
        {
            get { return _screenBounds; }
        }
        
        public SpriteBatch SpriteBatch { get; private set; }

        public DrawState(GraphicsDeviceManager graphics)
        {

            SpriteBatch = new SpriteBatch(graphics.GraphicsDevice);

            var screenScale = graphics.PreferredBackBufferHeight / 720.0f;

            _screenXform = Matrix.CreateScale(screenScale,
                screenScale, 1.0f);
                
            _screenBounds = new Rectangle(0, 0,
                (int)Math.Round(graphics.PreferredBackBufferWidth / screenScale),
                (int)Math.Round(graphics.PreferredBackBufferHeight / screenScale));
        }
    }
}
