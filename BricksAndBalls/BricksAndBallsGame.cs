﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Windows.UI.ViewManagement;
using BricksAndBalls.Screens;
using BricksAndBalls.Helpers;
using System;
using Microsoft.Xna.Framework.Input.Touch;
using Windows.Phone.UI.Input;
using Windows.UI.Popups;
namespace BricksAndBalls
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class BricksAndBallsGame : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        // Render state
        DrawState drawState;
      
             
        // Screens
        MenuScreen menuScreen;
        GameScreen gameScreen;
     
        public BricksAndBallsGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            HardwareButtons.BackPressed += OnBackButton;
        }

        private void OnBackButton(object sender, BackPressedEventArgs e)
        {
            if (gameScreen == null)
            {
                e.Handled = false;
                var dlg = new MessageDialog("Are you sure you want to exit the game?", "Quit");
                dlg.Commands.Add(new UICommand("Yes", command =>
                {
                    menuScreen.Unload();
                    this.Exit();
                }));
                dlg.Commands.Add(new UICommand("No"));
                dlg.CancelCommandIndex = 1;
                dlg.ShowAsync();
         
            }
            else
            {
                e.Handled = true;
                var dlg = new MessageDialog("Are you sure you want to return to the menu screen?", "Quit");
                dlg.Commands.Add(new UICommand("Yes", command =>
                {
                    gameScreen.Unload();
                    gameScreen = null;
                }));
                dlg.Commands.Add(new UICommand("No"));
                dlg.CancelCommandIndex = 1;
                dlg.ShowAsync();
         
            }
        
        }

    

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            graphics.SupportedOrientations = DisplayOrientation.LandscapeLeft | DisplayOrientation.LandscapeRight;
            
            // Hide the status bar.  Don't worry about this warning.
            StatusBar.GetForCurrentView().HideAsync();

            //Init the renderstate
            drawState = new DrawState(graphics);

            // Init the screens
            menuScreen = new MenuScreen(Content, drawState);
            menuScreen.StartButtonClicked += HandleStartClicked;

            TouchPanel.DisplayHeight = drawState.ScreenBounds.Height;
            TouchPanel.DisplayWidth = drawState.ScreenBounds.Width;
                     
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            // TODO: use this.Content to load your game content here
            menuScreen.LoadContent();
           
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
            menuScreen.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            var touchCollection = TouchPanel.GetState();

            // TODO: Add your update logic here
            if (gameScreen != null)
                gameScreen.Update(gameTime, ref touchCollection);
            else
                menuScreen.Update(gameTime, ref touchCollection);
           
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

          
         
            // The ending game code goes in here.
            if (gameScreen != null)
                gameScreen.Draw();
            else
                menuScreen.Draw();
        



            base.Draw(gameTime);
        }

        public void HandleStartClicked(object sender, EventArgs eventArgs)
        {
            // Do something intelligent when the point changes.  Perhaps redraw the GUI,
            // or update another data structure, or anything else you can think of.
           gameScreen = new GameScreen(Content, drawState);
           gameScreen.LoadContent();

        }


        public void HandleLeftClicked(object sender, EventArgs eventArgs)
        {
            //Move the bat to the left.
           
        }

        public void HandleRightClicked(object sender, EventArgs eventArgs)
        {
            // Move the bat to the right
        }
    }
}
