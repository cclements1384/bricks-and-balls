﻿using BricksAndBalls.GameObjects;
using BricksAndBalls.Helpers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BricksAndBalls.Screens
{
    class GameScreen
    {
        private DrawState _state;
        private ContentManager _content;
        private Texture2D backgroundTexture;
        private Rectangle _location;

        private MoveButton leftButton;
        private MoveButton rightButton;

        private Bat bat;
        private Vector2 batStartPosition;

        private Rectangle gamePort;

        private Vector2 _tempPosition;

       
        
         int bw = 2; // Border width
        Texture2D dummyTexture;


        public GameScreen(ContentManager content, DrawState drawState)
        {
            _state = drawState;
            _content = content;
            gamePort = new Rectangle(273, 78, 735, 560);

             dummyTexture = new Texture2D(_state.SpriteBatch.GraphicsDevice, 1, 1);
             dummyTexture.SetData(new Color[] { Color.Yellow });


          

        }

        private void InitializeBatStartPosition(Texture2D batTexture)
        {
            var batStartX = gamePort.GetCenterX() + gamePort.X - (batTexture.Width / 2);
            var batStartY = gamePort.Height + gamePort.Y - batTexture.Height;
            batStartPosition = new Vector2(batStartX,
                batStartY);
        }

        public void LoadContent()
        {
           backgroundTexture = _content
               .Load<Texture2D>("graphics\\background1");

           var leftButtonTexture = _content
               .Load<Texture2D>("graphics\\leftArrow");

           var rightButtonTexture = _content
               .Load<Texture2D>("graphics\\rightArrow");

            var batTexture = _content
                .Load<Texture2D>("graphics\\paddle");

            leftButton = new MoveButton(leftButtonTexture,
               45, 540);

            rightButton = new MoveButton(rightButtonTexture,
               1065, 540);

           InitializeBatStartPosition(batTexture);

           bat = new Bat(batTexture,
               (int)batStartPosition.X,
               (int)batStartPosition.Y);

           InitLocation();
        }

     

        public void Draw()
        {
            _state.SpriteBatch.Begin(SpriteSortMode.Deferred,
                null,
                null,
                null,
                null,
                null,
                _state.ScreenXform);

            // Draw here.
            _state.SpriteBatch.Draw(backgroundTexture,
                _location,
                Color.White);

            leftButton.Draw(_state.SpriteBatch);
            rightButton.Draw(_state.SpriteBatch);

            bat.Draw(_state.SpriteBatch);

            //DrawGamePort();   
            _state.SpriteBatch.End();


        }

        private void DrawGamePort()
        {
            _state.SpriteBatch.Draw(dummyTexture, new Rectangle(gamePort.Left, gamePort.Top, bw, gamePort.Height), Color.Red); // Left
            _state.SpriteBatch.Draw(dummyTexture, new Rectangle(gamePort.Right, gamePort.Top, bw, gamePort.Height), Color.Red); // Right
            _state.SpriteBatch.Draw(dummyTexture, new Rectangle(gamePort.Left, gamePort.Top, gamePort.Width, bw), Color.Red); // Top
            _state.SpriteBatch.Draw(dummyTexture, new Rectangle(gamePort.Left, gamePort.Bottom, gamePort.Width, bw), Color.Red); // Bottom

        }

        public void Update(GameTime gameTime, ref TouchCollection touches)
        {
            if(leftButton.WasPressed(ref touches))
            {
                // raise the left button pressed event.
                bat.MoveLeft(GamePortXBound(),
                    GamePortYBound());
            }

            if(rightButton.WasPressed(ref touches))
            {
                // raise the right button pressed event.
                bat.MoveRight(GamePortXBound(),
                    GamePortYBound());
            }

          
        }

        public void Unload()
        {
            backgroundTexture.Dispose();
        }

        private void InitLocation()
        {
            _location = new Rectangle(0,0,
                backgroundTexture.Width,
            backgroundTexture.Height);
        }

        public event EventHandler LeftButtonClicked;
        public event EventHandler RightButtonClicked;

        private void OnLeftButtonClicked()
        {
            
            //LeftButtonClicked(this, EventArgs.Empty);
        }

        private void OnRightButtonClicked()
        {
            //RightButtonClicked(this, EventArgs.Empty);
        }

        private int GamePortXBound()
        {
            return gamePort.X;
        }

        private int GamePortYBound()
        {
            return gamePort.Width + gamePort.X - bat.Width;
        }
    }
}
