﻿using BricksAndBalls.GameObjects;
using BricksAndBalls.Helpers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BricksAndBalls.Screens
{
    class MenuScreen
    {
        Rectangle _location;
        Texture2D menuTexture;
        private ContentManager _content;
        DrawState _state;

        Button startButton;

        public MenuScreen(ContentManager content, DrawState drawState)
        {
            _content = content;
            _state = drawState;

        }

        public void Draw()
        {

            var center = _state.ScreenBounds.Center.ToVector2();

            _state.SpriteBatch.Begin(SpriteSortMode.Deferred,
                null,
                null,
                null,
                null,
                null,
                _state.ScreenXform);

            _state.SpriteBatch.Draw(menuTexture,
                _location,
                null,
                Color.White);

            startButton.Draw(_state.SpriteBatch);

            _state.SpriteBatch.End();
        }

        public void Update(GameTime gameTime, ref TouchCollection touchCollection)
        {
            if (startButton.WasPressed(ref touchCollection))
                OnStartButtonClicked();
        }

        public void LoadContent()
        {
            menuTexture = _content
                .Load<Texture2D>("graphics\\menuScreen");

            var startButtonTexture = _content
                .Load<Texture2D>("graphics\\startButton");

            startButton = new Button(startButtonTexture,
                540, 450);

            Initialize();
        }

        public void Unload()
        {
            menuTexture.Dispose();
        }

        private void Initialize()
        {
            _location = new Rectangle(
                0, 
                0,
                menuTexture.Width,
                menuTexture.Height);
        }

        public event EventHandler StartButtonClicked;

        private void OnStartButtonClicked()
        {
            StartButtonClicked(this, EventArgs.Empty);
        }

    }
}
